package com.aavri.digitalbank.dao;

import javax.annotation.PostConstruct;

public abstract class SingletonDaoFactory implements DaoFactory {
    protected static DaoFactory INSTANCE;

    public static DaoFactory getInstance() {
        return INSTANCE;
    }

    @PostConstruct
    public void initialize() {
        init();
        INSTANCE = this;

    }

    protected abstract void init();
}
