package com.aavri.digitalbank.service;

import com.aavri.digitalbank.domain.BankBranch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BranchServiceTest {

    @Autowired
    private BranchService branchService;

    @Test
    public void test_findAll() {
        List<BankBranch> branchList = branchService.findAll();

        assertTrue(branchList.size() > 0);
    }

    @Test
    public void test_findByCity() {

        List<BankBranch> results = branchService.findByCity("Liverpool");
        assertTrue(results.size() > 0);
    }

    @Test
    public void test_findByCity_empty() {

        List<BankBranch> results = branchService.findByCity("XYZ");
        assertTrue(results.size() == 0);
    }
}
