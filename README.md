# Aavri REST Assessment Project Using SpringBoot, Jersey And Swagger

## Precondition

You need install:
- JDK 1.8.x
- Apache Maven 3.5.2


## Build

mvn clean install


## Start the Application

cd ./target
java -jar aavri.jar

Can directly access through the following api url or test through Postman or other REST test tool.


## API

localhost:8080/swagger-ui.html
localhost:8080/api/swagger.json


## Unit Test and Code Coverage Report

mvn clean test

unit test reports are under ./target/surefire-reports
code coverage reports are under ./target/jacoco-ut, open index.html with browser


## Contact

Please disclose any issues or questions by emailing [wen_qun@hotmail.com](mailto:wen_qun@hotmail.com).


## License


