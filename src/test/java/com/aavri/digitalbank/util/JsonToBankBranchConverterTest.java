package com.aavri.digitalbank.util;

import com.aavri.digitalbank.domain.BankBranch;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

public class JsonToBankBranchConverterTest {

    private static JsonToBankBranchConverter converter;

    private String json = "{\"Identification\":\"11116400\",\"SequenceNumber\":\"00\",\"Name\":\"GARFORTH\",\"Type\":\"Physical\",\"CustomerSegment\":[\"Personal\"],\"Availability\":{\"StandardAvailability\":{\"Day\":[{\"Name\":\"Monday\",\"OpeningHours\":[{\"OpeningTime\":\"09:00\",\"ClosingTime\":\"16:30\"}]},{\"Name\":\"Tuesday\",\"OpeningHours\":[{\"OpeningTime\":\"09:00\",\"ClosingTime\":\"16:30\"}]},{\"Name\":\"Wednesday\",\"OpeningHours\":[{\"OpeningTime\":\"09:30\",\"ClosingTime\":\"16:30\"}]},{\"Name\":\"Thursday\",\"OpeningHours\":[{\"OpeningTime\":\"09:00\",\"ClosingTime\":\"16:30\"}]},{\"Name\":\"Friday\",\"OpeningHours\":[{\"OpeningTime\":\"09:00\",\"ClosingTime\":\"16:30\"}]},{\"Name\":\"Saturday\",\"OpeningHours\":[{\"OpeningTime\":\"09:00\",\"ClosingTime\":\"13:00\"}]}]}},\"ContactInfo\":[{\"ContactType\":\"Phone\",\"ContactContent\":\"+44-1132150208\"},{\"ContactType\":\"Fax\",\"ContactContent\":\"+44-1132873667\"}],\"PostalAddress\":{\"AddressLine\":[\"GARFORTH 61 MAIN STREET\"],\"TownName\":\"LEEDS\",\"CountrySubDivision\":[\"WEST YORKSHIRE\"],\"Country\":\"GB\",\"PostCode\":\"LS25 1AF\",\"GeoLocation\":{\"GeographicCoordinates\":{\"Latitude\":\"53.795403\",\"Longitude\":\"-1.388505\"}}}}";

    @BeforeClass
    public static void beforeClass() {
        converter = new JsonToBankBranchConverter();
    }

    @Test
    public void test_fromJson() {
        BankBranch obj = converter.fromJson(json);

        assertNotNull(obj);
        assertEquals("GARFORTH", obj.getBanchName());
        assertEquals("GARFORTH 61 MAIN STREET", obj.getStreetAddress());
        assertEquals("LEEDS", obj.getCity());
        assertEquals("WEST YORKSHIRE", obj.getCountrySubDivision());
        assertEquals("GB", obj.getCountry());
        assertEquals("LS25 1AF", obj.getPostCode());
        assertEquals(53.795403f, obj.getLatitude(), 0.00001f);
        assertEquals(-1.388505f, obj.getLongitude(), 0.00001f);

    }

    @Test
    public void test_fromJson_null() {
        BankBranch obj = converter.fromJson(null);

        assertNull(obj);

    }

    @Test
    public void tes_fromJsonMessage() throws IOException {

        InputStream is = getClass().getClassLoader().getResourceAsStream("BranchMessage.json");
        String message = IOUtils.toString(is);
        List<BankBranch> list = converter.fromJsonMessage(message);

        assertTrue(list.size() > 0);
    }
}
