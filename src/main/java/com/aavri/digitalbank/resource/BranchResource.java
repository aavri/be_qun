package com.aavri.digitalbank.resource;

import com.aavri.digitalbank.domain.BankBranch;
import com.aavri.digitalbank.service.BranchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Api(value = "Bank Branch API")
@Path("/branch")
public class BranchResource {

    @Autowired
    private BranchService branchService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Retrieves all brank branches.", response = BankBranch[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BankBranch[].class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public Response listAll() {
        List<BankBranch> list = branchService.findAll();
        return Response.ok().entity(list).build();

    }

    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Find brank branches by city.", response = BankBranch[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = BankBranch[].class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public Response findByCity(@QueryParam("city") String city) {
        List<BankBranch> list = branchService.findByCity(city);
        return Response.ok().entity(list).build();

    }
}
