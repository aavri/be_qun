package com.aavri.digitalbank.dao.impl;

import com.aavri.digitalbank.dao.BranchDao;
import com.aavri.digitalbank.domain.BankBranch;
import com.aavri.digitalbank.util.JsonUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BranchDaoImpl extends AbstractDao implements BranchDao {


    @Override
    public List<BankBranch> findAll() {
        String json = null;
        try {
            json = run("https://api.halifax.co.uk/open-banking/v2.2/branches");

            return JsonUtils.fromJsonMessage(json, BankBranch.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<BankBranch> findByCity(String city) {
        List<BankBranch> result = new ArrayList<>();

        List<BankBranch> list = findAll();
        for(BankBranch branch: list) {
            if(branch.getCity() != null && branch.getCity().equalsIgnoreCase(city)) {
                result.add(branch);
            }
        }

        return result;
    }
}
