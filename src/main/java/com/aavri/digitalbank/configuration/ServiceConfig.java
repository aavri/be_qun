package com.aavri.digitalbank.configuration;

import com.aavri.digitalbank.dao.DaoCreationException;
import com.aavri.digitalbank.dao.DaoFactory;
import com.aavri.digitalbank.dao.SingletonDaoFactory;
import com.aavri.digitalbank.dao.impl.AbstractDao;
import com.aavri.digitalbank.service.BranchService;
import com.aavri.digitalbank.service.impl.DefaultBranchService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

    @Bean
    public BranchService branchService() {
        return new DefaultBranchService();
    }

    @Bean
    public DaoFactory daoFactory() {
        return new DefaultDaoFactory();
    }

    private class DefaultDaoFactory extends SingletonDaoFactory {

        private String packageName;
        private String DATASOURCE = "";

        @Override
        protected void init() {
            this.packageName = AbstractDao.class.getPackage().getName();
        }

        @Override
        public <T> T create(Class<T> daoType) throws DaoCreationException {
            try {
                String className = packageName + "." + daoType.getSimpleName() + "Impl";
                return (T) Class.forName(className).newInstance();

            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                throw new DaoCreationException(e);
            }
        }
    }
}
