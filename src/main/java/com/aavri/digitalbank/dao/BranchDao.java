package com.aavri.digitalbank.dao;

import com.aavri.digitalbank.domain.BankBranch;

import java.util.List;

public interface BranchDao {
    List<BankBranch> findAll();

    List<BankBranch> findByCity(String city);
}
