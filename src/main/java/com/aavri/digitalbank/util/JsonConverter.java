package com.aavri.digitalbank.util;

import java.util.List;

public interface JsonConverter<T> {

    T fromJson(String json);

    List<T> fromJsonMessage(String message);
}
