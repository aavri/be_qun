package com.aavri.digitalbank.service;

import com.aavri.digitalbank.domain.BankBranch;

import java.util.List;

public interface BranchService {
    List<BankBranch> findAll();

    List<BankBranch> findByCity(String city);
}
