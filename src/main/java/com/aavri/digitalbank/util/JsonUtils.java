package com.aavri.digitalbank.util;

import com.aavri.digitalbank.domain.BankBranch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtils {

    private static Gson gson;
    private static Map<Class, JsonConverter> converterMap;

    static {
        gson = new GsonBuilder().create();

        converterMap = new HashMap<>();
        converterMap.put(BankBranch.class, new JsonToBankBranchConverter());
    }

    private JsonUtils() {

    }

    public static <T> List<T> fromJsonMessage(String message, Class<T> type) {
        if(converterMap.containsKey(type)) {
            return  converterMap.get(type).fromJsonMessage(message);

        } else {
            return null;
        }

    }

    public static <T> T fromJson(String json, Class<T> type) {

        if(converterMap.containsKey(type)) {
            return (T) converterMap.get(type).fromJson(json);

        } else {
            return gson.fromJson(json, type);
        }

    }
}
