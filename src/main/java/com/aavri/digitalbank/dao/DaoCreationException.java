package com.aavri.digitalbank.dao;

public class DaoCreationException extends RuntimeException {

    public DaoCreationException() {
    }

    public DaoCreationException(String message) {
        super(message);
    }

    public DaoCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoCreationException(Throwable cause) {
        super(cause);
    }
}
