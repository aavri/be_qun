package com.aavri.digitalbank.configuration;

import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.LinkedHashMap;
import java.util.Map;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> , ApplicationEventListener {

    private static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);
    private ExceptionLogger exceptionLogger;

    @Override
    public Response toResponse(Exception exception) {
        int status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();

        if (exception instanceof WebApplicationException) {
            WebApplicationException wex = (WebApplicationException) exception;
            status = wex.getResponse().getStatus();

        }

        Map<String, Object> msg = new LinkedHashMap<>();
        msg.put("status", status);
        msg.put("exception", exception.getClass().getSimpleName());
        if (exception.getMessage() != null) {
            msg.put("message", exception.getMessage());
        }

        return Response.status(status).type(MediaType.APPLICATION_JSON_TYPE)
                .entity(msg).build();
    }

    @Override
    public void onEvent(ApplicationEvent applicationEvent) {
        if (ApplicationEvent.Type.INITIALIZATION_START.equals(applicationEvent.getType())) {
            exceptionLogger = new ExceptionLogger(LOGGER);
        }

    }

    @Override
    public RequestEventListener onRequest(RequestEvent requestEvent) {
        return new DefaultRequestEventLister();
    }

    class DefaultRequestEventLister implements RequestEventListener {

        @Override
        public void onEvent(RequestEvent requestEvent) {
            if (RequestEvent.Type.EXCEPTION_MAPPER_FOUND.equals(requestEvent.getType())) {
                exceptionLogger.onEvent(requestEvent);
            }

        }
    }
}
