package com.aavri.digitalbank.dao;

public interface DaoFactory {
    <T> T create(Class<T> daoType) throws DaoCreationException;
}
