package com.aavri.digitalbank.configuration;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;

public class ExceptionLogger implements RequestEventListener {
    private final Logger logger;

    public ExceptionLogger(Logger logger) {
        this.logger = logger;
    }

    public void onEvent(RequestEvent requestEvent) {
        ContainerRequest request = requestEvent.getContainerRequest();

        Throwable th = requestEvent.getException();

        StringBuilder builder = new StringBuilder(th.getClass().getName()).append(": ").append(th.getMessage());
        builder.append("\n").append(request.getMethod()).append(": ").append(request.getAbsolutePath());
        builder.append("\n").append("HEADERS: ").append(request.getHeaders());
        builder.append("\n").append("DETAILS: ").append("Data Message");

        logger.error(builder.toString(), th);
    }
}
