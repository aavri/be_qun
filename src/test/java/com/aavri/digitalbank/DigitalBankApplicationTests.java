package com.aavri.digitalbank;

import com.aavri.digitalbank.configuration.ExceptionHandler;
import com.aavri.digitalbank.dao.BranchDao;
import com.aavri.digitalbank.dao.DaoCreationException;
import com.aavri.digitalbank.dao.SingletonDaoFactory;
import com.aavri.digitalbank.resource.BranchResource;
import com.aavri.digitalbank.service.BranchService;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.activation.DataSource;
import javax.ws.rs.ext.ExceptionMapper;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DigitalBankApplicationTests {

	@Autowired(required = false)
	private BranchService branchService;

	@Autowired(required = false)
	private ResourceConfig resourceConfig;

	@Autowired(required = false)
	private DataSource dataSource;

	@Test
	public void contextLoads() {

		assertNull(dataSource);

		assertNotNull(SingletonDaoFactory.getInstance());

		assertNotNull(branchService);

	}

	@Test
	public void test_jerseyConfig() {

		assertNotNull(resourceConfig);

		Set<Class<?>> classes = resourceConfig.getClasses();

		assertTrue(classes.contains(BranchResource.class));
		assertTrue(classes.contains(ExceptionMapper.class));

	}

	@Test
	public void test_daoFactory_create_success() {
		BranchDao dao = SingletonDaoFactory.getInstance().create(BranchDao.class);
		assertNotNull(dao);
	}

	@Test(expected = DaoCreationException.class)
	public void test_daoFactory_create_failure() {
		DummyDao dao = SingletonDaoFactory.getInstance().create(DummyDao.class);
	}

	public interface DummyDao {

	}

}
