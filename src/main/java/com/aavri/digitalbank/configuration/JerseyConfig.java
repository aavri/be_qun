package com.aavri.digitalbank.configuration;

import com.aavri.digitalbank.resource.BranchResource;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ExceptionMapper;

@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        registerClasses(BranchResource.class, ExceptionMapper.class);
        swaggerConfig();
    }

    private void swaggerConfig() {
        this.register(ApiListingResource.class);
        this.register(SwaggerSerializers.class);

        BeanConfig swaggerConfigBean = new BeanConfig();
        swaggerConfigBean.setConfigId("Aavri REST Assessment");
        swaggerConfigBean.setTitle("Aavri REST Assessment Project Using SpringBoot, Jersey And Swagger");
        swaggerConfigBean.setVersion("v1");
        swaggerConfigBean.setContact("wen_qun@hotmail.com");
        swaggerConfigBean.setSchemes(new String[] { "http" });
        swaggerConfigBean.setBasePath("/api");
        swaggerConfigBean.setResourcePackage(BranchResource.class.getPackage().getName());
        swaggerConfigBean.setPrettyPrint(true);
        swaggerConfigBean.setScan(true);
    }

}