package com.aavri.digitalbank.util;

import com.aavri.digitalbank.domain.BankBranch;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

public class JsonToBankBranchConverter implements JsonConverter<BankBranch> {


    @Override
    public BankBranch fromJson(String json) {
        if (json == null) {
            return null;
        }

        JsonParser parser = new JsonParser();
        JsonElement je = parser.parse(json);
        JsonObject jo = je.getAsJsonObject();

        return fromJsonObject(jo);
    }

    @Override
    public List<BankBranch> fromJsonMessage(String message) {
        if (message == null) {
            return null;
        }

        List<BankBranch> list = new ArrayList<>();

        JsonParser parser = new JsonParser();
        JsonElement je = parser.parse(message);
        JsonObject jo = je.getAsJsonObject();

        JsonArray data = jo.get("data").getAsJsonArray();

        for (int i = 0; i < data.size(); i++) {
            JsonArray brands = data.get(i).getAsJsonObject().get("Brand").getAsJsonArray();

            for (int j = 0; j < brands.size(); j++) {
                JsonObject brand = brands.get(j).getAsJsonObject();

                JsonArray branches = brand.get("Branch").getAsJsonArray();
                for (int k = 0; k < branches.size(); k++) {
                    JsonObject branch = branches.get(k).getAsJsonObject();

                    list.add(fromJsonObject(branch));
                }
            }

        }

        return list;
    }

    private BankBranch fromJsonObject(JsonObject jo) {
        BankBranch bankBranch = new BankBranch();

        String name = jo.get("Name").getAsString();
        bankBranch.setBanchName(name);

        if (jo.get("PostalAddress") != null) {
            JsonObject addr = jo.get("PostalAddress").getAsJsonObject();
            bankBranch.setStreetAddress(addr.get("AddressLine").getAsString());
            if (addr.get("TownName") != null) {
                bankBranch.setCity(addr.get("TownName").getAsString());
            }
            if (addr.get("CountrySubDivision") != null) {
                bankBranch.setCountrySubDivision(addr.get("CountrySubDivision").getAsString());
            }
            if (addr.get("Country") != null) {
                bankBranch.setCountry(addr.get("Country").getAsString());
            }
            if (addr.get("PostCode") != null) {
                bankBranch.setPostCode(addr.get("PostCode").getAsString());
            }

            if (addr.get("GeoLocation") != null) {
                JsonObject geoLocation = addr.get("GeoLocation").getAsJsonObject();
                if (geoLocation.get("GeographicCoordinates") != null) {
                    JsonObject geo = geoLocation.get("GeographicCoordinates").getAsJsonObject();
                    bankBranch.setLatitude(geo.get("Latitude").getAsFloat());
                    bankBranch.setLongitude(geo.get("Longitude").getAsFloat());
                }
            }
        }

        return bankBranch;

    }


}
