package com.aavri.digitalbank.dao;

import com.aavri.digitalbank.dao.impl.BranchDaoImpl;
import com.aavri.digitalbank.domain.BankBranch;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class BranchDaoTest {
    private static BranchDao dao;

    @BeforeClass
    public static void beforeClass() {
        dao = new BranchDaoImpl();
    }

    @Test
    public void test_findAll() {
        List<BankBranch> results = dao.findAll();

        assertNotNull(results);
        assertTrue(results.size() > 0);
    }

    @Test
    public void test_findByCity() {
        List<BankBranch> results = dao.findByCity("LiverPool");

        assertNotNull(results);
        assertTrue(results.size() > 0);
    }

    @Test
    public void test_findByCity_empty() {
        List<BankBranch> results = dao.findByCity("Liver Pool");

        assertNotNull(results);
        assertTrue(results.size() == 0);
    }
}
