package com.aavri.digitalbank.service.impl;

import com.aavri.digitalbank.dao.BranchDao;
import com.aavri.digitalbank.dao.SingletonDaoFactory;
import com.aavri.digitalbank.domain.BankBranch;
import com.aavri.digitalbank.service.BranchService;

import java.util.List;

public class DefaultBranchService implements BranchService {

    @Override
    public List<BankBranch> findAll() {
        return SingletonDaoFactory.getInstance().create(BranchDao.class).findAll();
    }

    @Override
    public List<BankBranch> findByCity(String city) {
        return SingletonDaoFactory.getInstance().create(BranchDao.class).findByCity(city);
    }
}
